#JSGF V1.0;
grammar speechRecognition;

public <sentence> 		= ([could you | robot | tobi] [please] <VP1> [([and|then] <VP2>)] [([and|then] <VP3>)]);

<VP1> 				= (<SingleCommands> | <NeedsToBeFirst> | <NoRestriction>);
<VP2> 				= (<NeedsReference> | <NoRestriction>);
<VP3> 				= (<NeedsReference> | <NoRestriction>);
 
<NeedsToBeFirst> 		= (<goToLoc> | <meetPrsAtBeac> | <findPrsInRoom>);
<SingleCommands> 		= (<countObjOnPlcmt> | <countPrsInRoom> | <tellPrsInfoInLoc> | <tellObjPropOnPlcmt> | <deliverObjToGestPrsInRoom> | <followNameFromBeacToRoom> | <guideNameFromBeacToBeac> | <guideNameToBeacFindAtBeac> | <bringMeObjFromPlcmt> | <tellCatPropOnPlcmt> | <findObjInRoom> | <talkInfoToGestPrsInRoom> | <answerToGestPrsInRoom>);
<NeedsReference> 		= <followPrsToRoom> | (<followPrs> | <placeObjPronOnPlcmt> | <deliverObjPronToMe> | <guidePrsToBeacon> | <deliverObjToNameAtBeac> | <answerQuestion> | <talkInfo> | <meetName> | <findPrs> | <placeObjOnPlcmt>);
<NoRestriction> 		= (<findObj>  | <takeObj> | <takeObjFromPlcmt>  | <bringMeObj> | <deliverObjToMe>); 


<goToLoc> 			= <V_go> <toLocPrep> (<NP_room> | <NP_placement> | <NP_beacon>);

<findObj> 			= <V_find> <NP_object>;
<findObjInRoom> 		= <V_find> (<NP_object> | <NP_category>) <inLocPrep> <NP_room>;

<takeObj> 			= <V_take> <NP_object>;
<takeObjFromPlcmt> 		= <V_take>  <NP_object> <fromLocPrep> (<NP_room> | <NP_placement>);

<findPrs> 			= <V_find> (<NP_gestperson> | <NP_genderperson> | <NP_poseperson>);
<findPrsInRoom> 		= <V_find> (<NP_gestperson> | <NP_genderperson> | <NP_poseperson>) <inLocPrep> <NP_room>;

<placeObjOnPlcmt> 		= <V_place> <NP_object> <placePrep>  <NP_placement>;
<placeObjPronOnPlcmt> 		= <V_place> <NP_objectpron> <placePrep>  <NP_placement>;

<talkInfo> 			= <V_talk> <NP_talk>;
<talkInfoToGestPrsInRoom> 	= <V_talk> <NP_talk> <talkPrep> <NP_gestperson> <inLocPrep> <NP_room>;

<answerQuestion> 		= <V_answer> <NP_answer>;
<answerToGestPrsInRoom> 	= <V_answer> <NP_answer> <talkPrep> <NP_gestperson> <inLocPrep> <NP_room>;

<followPrs> 			= <V_follow> <NP_personalpron>;
<followPrsToRoom> 		= <V_follow> <NP_personalpron> <toLocPrep> <NP_roomTwo>;
<followNameFromBeacToRoom> 	= <V_follow> <NP_name> <fromLocPrep> <NP_beacon> <toLocPrep> <NP_roomTwo>;

<meetName> 			= <V_meet> <NP_name>;
<meetPrsAtBeac> 		= <V_meet> <NP_name> <atLocPrep> <NP_beacon>;

<bringMeObj>    		= <V_bring> <NP_me> <NP_object>;
<bringMeObjFromPlcmt> 		= <V_bring> <NP_me> <NP_object> <fromLocPrep> <NP_placement>;

<countObjOnPlcmt> 		= <V_count> (<NP_object> | <NP_category>) there are <placePrep> <NP_placement>;
<countPrsInRoom> 		= <V_count> people in <NP_room> <arePrep> (<NP_posture> | <NP_genderpersonplural>);

<tellPrsInfoInLoc> 		= <V_tell> me <NP_personattribute> of the person <LocPrep> (<NP_room> | <NP_beacon>);
<tellObjPropOnPlcmt> 		= <V_tell> me ((what is) | whats) <NP_objectprop> object <onLocPrep> <NP_placement>;
<tellCatPropOnPlcmt> 		= <V_tell> me ((what is) | whats) <NP_objectprop> <NP_category> <onLocPrep> <NP_placement>;

<deliverObjToMe> 		= <V_deliver> <NP_object> <deliverPrep> <NP_me>;
<deliverObjPronToMe> 		= <V_deliver> <NP_objectpron> <deliverPrep> <NP_me>;
<deliverObjToGestPrsInRoom> 	= <V_deliver> <NP_object> <deliverPrep> <NP_gestperson> <inLocPrep> <NP_roomTwo>;
<deliverObjToNameAtBeac> 	= <V_deliver> <NP_objectpron> <deliverPrep> <NP_name> <atLocPrep> <NP_beaconTwo>;

<guidePrsToBeacon> 		= <V_guide> <NP_personalpron> <toLocPrep> <NP_beaconTwo>;
<guideNameFromBeacToBeac> 	= <V_guide> <NP_name> <fromLocPrep> <NP_beacon> <toLocPrep> <NP_beaconTwo>;
<guideNameToBeacFindAtBeac> 	= <V_guide> <NP_name> <toLocPrep> <NP_beaconTwo> you (may | can | will) find (them | him | her) <atLocPrep> <NP_beacon>;


<deliverPrep> 			= to;
<placePrep> 			= on;
<inLocPrep> 			= in;
<fromLocPrep> 			= from;
<toLocPrep> 			= to;
<atLocPrep> 			= at;
<talkPrep> 			= to;
<LocPrep> 			= in | at;
<onLocPrep> 			= on;
<arePrep> 			= are;

<V_take>    			= bring | take | get | grasp | (pick up);
<V_place>   			= put | place;
<V_deliver> 			= bring | give | deliver;
<V_bring> 			= bring | give;
<V_go> 				= go | navigate;
<V_find> 			= find | locate | (look for);
<V_talk> 			= tell | say;
<V_answer> 			= answer;
<V_follow> 			= follow;
<V_meet> 			= meet;
<V_guide> 			= guide | escort | take | lead | accompany;
<V_tell> 			= tell;
<V_count> 			= tell me how many;

<NP_object>    			= (the <N_object>) | (a <N_object>) | (an <N_object>) | (<N_object>);
<NP_objectpron> 		= (<N_objectpron>);
<NP_category> 			= (the <N_category>) | <N_category>;
<NP_objectprop> 		= (the <N_objectprop>);
<NP_placement> 			= (the <N_placement>);
<NP_placementTwo> 		= (the <N_placementTwo>);
<NP_beacon> 			= (the <N_beacon>);
<NP_beaconTwo> 			= (the <N_beaconTwo>);
<NP_room> 			= (the <N_room>);
<NP_roomTwo> 			= (the <N_roomTwo>);
<NP_me>        			= (<N_me>);
<NP_gestperson> 		= (the <N_gestperson>) | (a <N_gestperson>);
<NP_poseperson> 		= (a <N_poseperson>);
<NP_genderperson> 		= (a <N_genderperson>);
<NP_genderpersonplural> 	= (<N_genderpersonplural>);
<NP_name> 			= <N_name>;
<NP_talk> 			= <N_talk>;
<NP_answer> 			= <N_answer>;
<NP_personalpron> 		= <N_personalpron>;
<NP_personattribute> 		= (the <N_personattribute>);
<NP_posture> 			= <N_posture>;

<N_objectpron> 			= it;
<N_objectprop> 			= biggest | largest | smallest | heaviest | lightest | thinnest;
<N_me>        			= me;
<N_gestperson> 			= (person waving) | (person (raising | rising) [their] (left|right) arm) | (person pointing to the (left|right)) | (person calling);
<N_poseperson> 			= (person sitting) | (person standing) | (person lying down);
<N_genderperson> 		= man | woman | boy | girl | (male person) | (female person);
<N_genderpersonplural> 		= men | women | boys | girls | male | female;
<N_talk> 			= (something about yourself) | (the time) | (what day is (today | tomorrow)) | (your teams (name | country | affiliation)) | (the day of the (week | month)) | (a joke);
<N_answer> 			= (a question);
<N_personalpron> 		= them | her | him;
<N_personattribute> 		= name | gender | pose;
<N_posture> 			= sitting | standing | lying down;

<N_object>    			= basket | cereals | cloth | (coconut milk) | coke | cornflakes | noodles | orange (juice | drink) | peas | plate | pringles | red bowl | salt | soap | sponge | (tomato paste) | water;
<N_category> 			= care | drink | (cleaning stuff) | container | food | snack | cutlery;
<N_room> 			= corridor | (living room) | kitchen | (bath room);
<N_roomTwo> 			= corridor | (living room) | kitchen | (bath room);
<N_placement> 			= (living table) | (kitchen counter) | sink | stove | cabinet | bar;
<N_placementTwo> 		= (living table) | (kitchen counter) | sink | stove | cabinet | bar;
<N_beacon> 			= (living table) | sink | bar | sofa | fridge | (comfy chair);
<N_beaconTwo> 			= (living table) | sink | bar | sofa | fridge | (comfy chair);
<N_name> 			= emma | olivia | sophia | ava | isabella | mia | abigail | emily | charlotte | harper | noah | liam | mason | jacob | william | ethan | james | alexander | michael | benjamin;
